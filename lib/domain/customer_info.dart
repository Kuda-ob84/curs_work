
class CustomerInfo {
  final String uid;
  final String email;
  final String password;
  final String name;
  final int balance;

  CustomerInfo({this.uid, this.email, this.password, this.name, this.balance});

  factory CustomerInfo.fromFirebase(Map<String, dynamic> doc) => CustomerInfo(
      uid: doc["uid"],
      email: doc["email"],
      password: doc["password"],
      name: doc["name"],
      balance: doc["balance"]);

  Map<String, dynamic> toMap() {
    return {
      "uid": uid,
      "email": email,
      "name": name,
      "password": password,
      "balance": balance
    };
  }
}
