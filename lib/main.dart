import 'package:curs_work/domain/customer.dart';
import 'package:curs_work/screens/landing_screen/landing.dart';
import 'package:curs_work/services/auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return StreamProvider<Customer>.value(
      value: AuthService().currentCustomer,
      child: MaterialApp(
        theme: ThemeData(primaryColor: Colors.blue),
        home: LandingPage(),
      ),
    );
  }
}
