// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

DTOOdds dtoOddsFromJson(String str) => DTOOdds.fromJson(json.decode(str));

String dtoOddsToJson(DTOOdds data) => json.encode(data.toJson());

class DTOOdds {
  DTOOdds({
    @required this.success,
    @required this.data,
  });

  final bool success;
  final List<Datum> data;

  factory DTOOdds.fromJson(Map<String, dynamic> json) => DTOOdds(
        success: json["success"] == null ? null : json["success"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    @required this.sportKey,
    @required this.sportNice,
    @required this.teams,
    @required this.homeTeam,
    @required this.commenceTime,
    @required this.sites,
    @required this.sitesCount,
  });

  final String sportKey;
  final String sportNice;
  final List<String> teams;
  final String homeTeam;
  final DateTime commenceTime;
  final List<Site> sites;
  final int sitesCount;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        sportKey: json["sport_key"] == null ? null : json["sport_key"],
        sportNice: json["sport_nice"] == null ? null : json["sport_nice"],
        teams: json["teams"] == null
            ? null
            : List<String>.from(json["teams"].map((x) => x)),
        homeTeam: json["home_team"] == null ? null : json["home_team"],
        commenceTime: json["commence_time"] == null
            ? null
            : DateTime.parse(json["commence_time"]),
        sites: json["sites"] == null
            ? null
            : List<Site>.from(json["sites"].map((x) => Site.fromJson(x))),
        sitesCount: json["sites_count"] == null ? null : json["sites_count"],
      );

  Map<String, dynamic> toJson() => {
        "sport_key": sportKey == null ? null : sportKey,
        "sport_nice": sportNice == null ? null : sportNice,
        "teams": teams == null ? null : List<dynamic>.from(teams.map((x) => x)),
        "home_team": homeTeam == null ? null : homeTeam,
        "commence_time":
            commenceTime == null ? null : commenceTime.toIso8601String(),
        "sites": sites == null
            ? null
            : List<dynamic>.from(sites.map((x) => x.toJson())),
        "sites_count": sitesCount == null ? null : sitesCount,
      };
}

class Site {
  Site({
    @required this.siteKey,
    @required this.siteNice,
    @required this.lastUpdate,
    @required this.odds,
  });

  final String siteKey;
  final String siteNice;
  final DateTime lastUpdate;
  final Map<String, List<double>> odds;

  factory Site.fromJson(Map<String, dynamic> json) => Site(
        siteKey: json["site_key"] == null ? null : json["site_key"],
        siteNice: json["site_nice"] == null ? null : json["site_nice"],
        lastUpdate: json["last_update"] == null
            ? null
            : DateTime.parse(json["last_update"]),
        odds: json["odds"] == null
            ? null
            : Map.from(json["odds"]).map((k, v) =>
                MapEntry<String, List<double>>(
                    k, List<double>.from(v.map((x) => x.toDouble())))),
      );

  Map<String, dynamic> toJson() => {
        "site_key": siteKey == null ? null : siteKey,
        "site_nice": siteNice == null ? null : siteNice,
        "last_update": lastUpdate == null ? null : lastUpdate.toIso8601String(),
        "odds": odds == null
            ? null
            : Map.from(odds).map((k, v) => MapEntry<String, dynamic>(
                k, List<dynamic>.from(v.map((x) => x)))),
      };
}
