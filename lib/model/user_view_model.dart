
class UserViewModel {
  int id;
  String roleId;
  String guid;
  String name;
  String avatar;
  dynamic settings;
  String email;
  String phone;
  String iin;
  int isPushEnabled;
  String satuDeviceToken;
  String satuDevicePlatform;
  String createdAt;
  String updatedAt;
  int isPartner;
  int debugMode;
  int realEstateAgencyId;
  int locationId;
  int isQr;

  // UserViewModel(DTOGetUserResponse dtoUser) {
  //   this.id = dtoUser?.user?.id ?? 0;
  //   this.roleId = dtoUser?.user?.roleId ?? '';
  //   this.guid = dtoUser?.user?.guid ?? '';
  //   this.name = dtoUser?.user?.name ?? '';
  //   this.avatar = dtoUser?.user?.avatar ?? '';
  //   this.settings = dtoUser?.user?.settings;
  //   this.email = dtoUser?.user?.email ?? '';
  //   this.phone = dtoUser?.user?.phone ?? '';
  //   this.iin = dtoUser?.user?.iin ?? '';
  //   this.isPushEnabled = dtoUser?.user?.isPushEnabled;
  //   this.satuDeviceToken = dtoUser?.user?.satuDeviceToken ?? '';
  //   this.satuDevicePlatform = dtoUser?.user?.satuDevicePlatform ?? '';
  //   this.createdAt = dtoUser?.user?.createdAt?.toDateFormat ?? "";
  //   this.updatedAt = dtoUser?.user?.updatedAt?.toDateFormat ?? "";
  //   this.isPartner = dtoUser?.user?.isPartner;
  //   this.debugMode = dtoUser?.user?.debugMode;
  //   this.realEstateAgencyId = dtoUser?.user?.realEstateAgencyId;
  //   this.locationId = dtoUser?.user?.locationId;
  //   this.isQr = dtoUser?.user?.isQr;
  // }
}
