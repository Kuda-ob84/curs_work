import 'package:curs_work/screens/authorization_screen/bloc/authorization_screen_bloc.dart';
import 'package:curs_work/screens/cipher_check/cipher_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AuthorizationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return BlocProvider<AuthorizationScreenBloc>(
        create: (context) =>
            AuthorizationScreenBloc()..add(InititalAuthorizationScreenEvent()),
        child: MaterialApp(
            home: DefaultTabController(
                length: 2,
                child: Scaffold(
                  backgroundColor: Colors.blueGrey,
                  appBar: AppBar(
                    elevation: 0,
                    toolbarHeight: 120,
                    backgroundColor: Colors.blueGrey,
                    centerTitle: true,
                    title: Text("Authorization screen"),
                    bottom: PreferredSize(
                      preferredSize: Size.fromHeight(50),
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(color: Colors.yellow)),
                        child: TabBar(
                            indicator: BoxDecoration(
                              color: Colors.yellow,
                              borderRadius: BorderRadius.circular(9),
                            ),
                            unselectedLabelColor: Colors.yellow,
                            labelColor: Colors.black,
                            tabs: [
                              Tab(
                                child: Text("EMAIL"),
                              ),
                              Tab(child: Text("PHONE"))
                            ]),
                      ),
                    ),
                  ),
                  body: BlocBuilder<AuthorizationScreenBloc,
                      AuthorizationScreenState>(builder: (context, state) {
                    if (state is InitialAuthorizationScreenState) {
                      return _buildScaffoldBody(context, ctx);
                    }
                    return Container();
                  }),
                ))));
  }

  Widget _buildScaffoldBody(BuildContext context, ctx) {
    return TabBarView(children: [
      SingleChildScrollView(
        child: Container(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 18, vertical: 25),
          child: Column(children: [
            _customTextField(
                "Email",
                BlocProvider.of<AuthorizationScreenBloc>(context)
                    .emailController,
                Icon(Icons.email),
                false),
            BlocProvider.of<AuthorizationScreenBloc>(context).isLogin
                ? SizedBox()
                : _customTextField(
                    "Name",
                    BlocProvider.of<AuthorizationScreenBloc>(context)
                        .nameController,
                    Icon(Icons.info),
                    false),
            _customTextField(
                "Password",
                BlocProvider.of<AuthorizationScreenBloc>(context)
                    .passwordController,
                Icon(Icons.lock),
                true),
            BlocProvider.of<AuthorizationScreenBloc>(context).isLogin
                ? SizedBox()
                : _customTextField(
                    "Confirm password",
                    BlocProvider.of<AuthorizationScreenBloc>(context)
                        .confirmPasswordController,
                    Icon(Icons.lock),
                    true),
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Colors.blue,
                  ),
                  child:
                      BlocProvider.of<AuthorizationScreenBloc>(context).isLogin
                          ? _buildButton("LOGIN", () {
                              BlocProvider.of<AuthorizationScreenBloc>(context)
                                ..add(LoginAuthorizationScreenEvent());
                            })
                          : _buildButton("REGISTER", () {
                              BlocProvider.of<AuthorizationScreenBloc>(context)
                                ..add(RegisterScreenEvent());
                            })),
            ),
            Padding(
                padding: const EdgeInsets.only(top: 18.0),
                child: BlocProvider.of<AuthorizationScreenBloc>(context).isLogin
                    ? _buildInkWell("Not registered yet? Register!", context)
                    : _buildInkWell("Already registered? Login!", context)),
            FlatButton(
                onPressed: () => Navigator.push(
                    context, MaterialPageRoute(builder: (_) => CipherScreen())),
                child: Text("Chipher check"))
          ]),
        )),
      ),
      Container(
          child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 18, vertical: 25),
        child: Column(children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: TextField(
                inputFormatters: [
                  BlocProvider.of<AuthorizationScreenBloc>(context)
                      .phoneFormatter
                ],
                controller: BlocProvider.of<AuthorizationScreenBloc>(context)
                    .phoneController,
                decoration: InputDecoration(
                    prefixIcon: Icon(Icons.phone),
                    hintText: "+7 (777) 777-77-77",
                    border: OutlineInputBorder())),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Colors.blue,
                  ),
                  child: _buildButton(
                      "LOGIN",
                      () => BlocProvider.of<AuthorizationScreenBloc>(context)
                        ..add(PhoneAuthorizationScreenEvent(context))))),
        ]),
      ))
    ]);
  }

  Widget _buildInkWell(String text, BuildContext context) {
    return InkWell(
        child: Text(
          text,
          style: TextStyle(fontSize: 16),
        ),
        onTap: () => BlocProvider.of<AuthorizationScreenBloc>(context)
          ..add(ChangeAuthorizationTypeScreenEvent()));
  }

  Widget _buildButton(String text, Function function) {
    return FlatButton(
        child: Text(text, style: TextStyle(color: Colors.white, fontSize: 16)),
        onPressed: function);
  }

  Widget _customTextField(
    String hintText,
    TextEditingController textEditingController,
    Icon icon,
    bool obscureText,
  ) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: TextField(
          obscureText: obscureText,
          controller: textEditingController,
          decoration: InputDecoration(
              prefixIcon: icon,
              hintText: hintText,
              border: OutlineInputBorder())),
    );
  }
}
