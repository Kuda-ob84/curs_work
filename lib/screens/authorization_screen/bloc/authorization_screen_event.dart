part of 'authorization_screen_bloc.dart';

abstract class AuthorizationScreenEvent extends Equatable {
  const AuthorizationScreenEvent();
  @override
  List<Object> get props => [];
}

class InititalAuthorizationScreenEvent extends AuthorizationScreenEvent {}

class ChangeAuthorizationTypeScreenEvent extends AuthorizationScreenEvent {}

class PhoneAuthorizationScreenEvent extends AuthorizationScreenEvent {
  final BuildContext context;

  PhoneAuthorizationScreenEvent(this.context);

  @override
  List<Object> get props => [];
}

class LoginAuthorizationScreenEvent extends AuthorizationScreenEvent {

}

class RegisterScreenEvent extends AuthorizationScreenEvent{

}