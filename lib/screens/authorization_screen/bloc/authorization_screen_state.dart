part of 'authorization_screen_bloc.dart';

@immutable
abstract class AuthorizationScreenState extends Equatable {
  const AuthorizationScreenState();
  @override
  List<Object> get props => [];
}

class InitialAuthorizationScreenState extends AuthorizationScreenState {}

class LoadingAuthorizationScreenState extends AuthorizationScreenState {}

class EmptyState extends AuthorizationScreenState {}
