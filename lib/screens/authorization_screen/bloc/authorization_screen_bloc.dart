import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curs_work/domain/cipher_help.dart';
import 'package:curs_work/domain/customer.dart';
import 'package:curs_work/domain/customer_info.dart';
import 'package:curs_work/services/auth.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:meta/meta.dart';
part 'authorization_screen_event.dart';
part 'authorization_screen_state.dart';

class AuthorizationScreenBloc
    extends Bloc<AuthorizationScreenEvent, AuthorizationScreenState> {
  AuthorizationScreenBloc() : super(InitialAuthorizationScreenState());
  AuthService _authService = AuthService();
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  String email;
  String password;
  String name;
  String phone;
  String code;
  MaskTextInputFormatter phoneFormatter = MaskTextInputFormatter(
      mask: "+# (###) ###-##-##", filter: {"#": RegExp(r'[0-9]')});
  bool isLogin = true;
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController confirmPasswordController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController codeController = new TextEditingController();
  @override
  Stream<AuthorizationScreenState> mapEventToState(
    AuthorizationScreenEvent event,
  ) async* {
    if (event is InititalAuthorizationScreenEvent) {
      yield* _buildInititalAuthorizationScreenEvent();
    } else if (event is ChangeAuthorizationTypeScreenEvent) {
      yield* _buildChangeAuthorizationTypeScreenEvent();
    } else if (event is PhoneAuthorizationScreenEvent) {
      yield* _buildPhoneAuthorizationScreenEvent(event.context);
    } else if (event is RegisterScreenEvent) {
      yield* _buildRegisterScreenEvent();
    } else if (event is LoginAuthorizationScreenEvent) {
      yield* _buildLoginAuthorizationScreenEvent();
    }
  }

  Stream<AuthorizationScreenState>
      _buildInititalAuthorizationScreenEvent() async* {}

  Stream<AuthorizationScreenState> _buildPhoneAuthorizationScreenEvent(
      BuildContext context) async* {
    phone = phoneController.text.trim();
    _authService.signUpWithPhone(phone, context, codeController);
  }

  Stream<AuthorizationScreenState>
      _buildChangeAuthorizationTypeScreenEvent() async* {
    isLogin = !isLogin;
    emailController.clear();
    passwordController.clear();
    yield EmptyState();
    yield InitialAuthorizationScreenState();
  }

  Stream<AuthorizationScreenState> _buildRegisterScreenEvent() async* {
    print("Register");
    email = emailController.text;
    password = passwordController.text;

    if (password != confirmPasswordController.text ||
        email.isEmpty ||
        password.isEmpty ||
        nameController.text.isEmpty) return;

    Customer customer = await _authService.signUpWithEmailAndPassword(
        email.trim(), password.trim(), nameController.text);

    if (customer == null) {
      Fluttertoast.showToast(
          msg: "Check your email/password",
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.red,
          textColor: Colors.white);
    }
  }

  Stream<AuthorizationScreenState>
      _buildLoginAuthorizationScreenEvent() async* {
    print("Login");
    email = emailController.text;
    password = passwordController.text;

    if (email.isEmpty || password.isEmpty) {
      return;
    }

    Customer customer = await _authService.signInWithEmailAndPassword(
        email.trim(), password.trim());
    if (customer == null) {
      Fluttertoast.showToast(
          msg: "User does not exist",
          gravity: ToastGravity.CENTER,
          backgroundColor: Colors.red,
          textColor: Colors.white);
    } else {
      emailController.clear();
      passwordController.clear();
    }
  }
}
