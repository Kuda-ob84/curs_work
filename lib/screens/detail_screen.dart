import 'package:cached_network_image/cached_network_image.dart';
import 'package:curs_work/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DetailScreen extends StatelessWidget {
  final _img;
  final _title;
  final _date;
  final _description;
  final _origin;

  DetailScreen(
      this._img, this._title, this._date, this._description, this._origin);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: Widgets().buildAppBar(context, _origin),
      body: new Container(
        margin: new EdgeInsets.all(10.0),
        child: new Material(
          elevation: 1,
          borderRadius: new BorderRadius.circular(6.0),
          child: new ListView(
            children: <Widget>[
              new Hero(tag: _title, child: CachedNetworkImage(imageUrl: _img)),
              _getBody(_title, _date, _description, _origin, context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _getBody(tittle, date, description, origin, context) {
    return new Container(
      margin: new EdgeInsets.all(15.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _getTittle(tittle),
          _getDate(date, origin),
          _getDescription(description),
        ],
      ),
    );
  }

  Widget _getLink(link, context) {
    return new GestureDetector(
      child: new Text(
        link,
        style: new TextStyle(color: Colors.blue),
      ),
      onTap: () {},
    );
  }

  _getTittle(tittle) {
    return new Text(
      tittle,
      style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
    );
  }

  _getDate(date, origin) {
    return new Container(
      margin: new EdgeInsets.only(top: 4.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Text(
            DateFormat("yyyy-MM-dd").format(date),
            style: new TextStyle(fontSize: 10.0, color: Colors.grey),
          ),
          new Text(
            origin,
            style: new TextStyle(fontSize: 10.0, color: Colors.grey),
          )
        ],
      ),
    );
  }

  _getDescription(description) {
    return new Container(
      margin: new EdgeInsets.only(top: 20.0),
      child: new Text(description),
    );
  }
}
