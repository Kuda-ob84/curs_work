import 'package:curs_work/domain/cipher_help.dart';
import 'package:flutter/material.dart';

class CipherScreen extends StatefulWidget {
  @override
  _CipherScreenState createState() => _CipherScreenState();
}

class _CipherScreenState extends State<CipherScreen> {
  TextEditingController textEditingController = TextEditingController();
  TextEditingController cipherEditingController = TextEditingController();
  TextEditingController decipherEditingController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Cipher check"), centerTitle: true),
        body: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _customTextField("Text", textEditingController,
                    Icon(Icons.text_fields), false),
                _customTextField("Cipher text", cipherEditingController,
                    Icon(Icons.lock), false),
                _customTextField("Decipher text", decipherEditingController,
                    Icon(Icons.lock_open), false),
                _buildButton("Cipher", () {
                  cipherEditingController.clear();
                  decipherEditingController.clear();
                  cipherEditingController.text =
                      encryptAESCryptoJS(textEditingController.text);
                }),
                _buildButton("Decipher", () {
                  try {
                    cipherEditingController.clear();
                    decipherEditingController.clear();
                    decipherEditingController.text =
                        decryptAESCryptoJS(textEditingController.text);
                  } catch (e) {
                    showDialog(
                        context: context,
                        child: AlertDialog(content: Text(e.toString())));
                  }
                })
              ]),
        ));
  }

  Widget _customTextField(
      String hintText,
      TextEditingController textEditingController,
      Icon icon,
      bool obscureText) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: TextField(
          obscureText: obscureText,
          controller: textEditingController,
          decoration: InputDecoration(
              prefixIcon: icon,
              hintText: hintText,
              border: OutlineInputBorder())),
    );
  }

  Widget _buildButton(String text, Function function) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Colors.blue,
      ),
      child: FlatButton(
          onPressed: function,
          child: Container(
              child: Text(text,
                  style: TextStyle(color: Colors.white, fontSize: 18)))),
    );
  }
}
