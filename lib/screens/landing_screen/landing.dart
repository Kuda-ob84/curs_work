import 'package:curs_work/domain/customer.dart';
import 'package:curs_work/screens/authorization_screen/auth.dart';
import 'package:curs_work/screens/home_screen/home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Customer customer = Provider.of<Customer>(context);
    final bool isLoggedIn = customer != null;
    return isLoggedIn ? HomeScreen() : AuthorizationScreen();
  }
}
