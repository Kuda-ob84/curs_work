import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curs_work/domain/customer_info.dart';
import 'package:curs_work/domain/news.dart';
import 'package:curs_work/network/news_dto.dart';
import 'package:curs_work/screens/authorization_screen/bloc/authorization_screen_bloc.dart';
import 'package:curs_work/screens/detail_screen.dart';
import 'package:curs_work/screens/home_screen/bloc/home_screen_bloc.dart';
import 'package:curs_work/services/auth.dart';
import 'package:curs_work/theme/main_theme.dart';
import 'package:curs_work/widgets/dialog.dart';
import 'package:curs_work/widgets/news_item_widget.dart';
import 'package:curs_work/widgets/widgets.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';

class HomeScreen extends StatelessWidget {
  final GlobalKey<InnerDrawerState> _innerDrawerKey =
      GlobalKey<InnerDrawerState>();
  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeScreenBloc>(
        create: (context) => HomeScreenBloc()..add(InitialHomeScreenEvent()),
        child: Container(child: BlocBuilder<HomeScreenBloc, HomeScreenState>(
            builder: (context, state) {
          if (state is LoadDataHomeScreenState)
            return _buildScaffoldBody(
                state.user, state.articles, state.customer, context);
          else if (state is LoadingHomeScreenState) {
            return Scaffold(
                appBar: Widgets().buildAppBar(context, "Loading"),
                body: Center(child: CircularProgressIndicator()));
          }
          return Container();
        })));
  }

  Widget _buildScaffoldBody(User customer, List<Article> articles,
      CustomerInfo customerInfo, context) {
    return InnerDrawer(
        key: _innerDrawerKey,
        onTapClose: true,
        swipe: true,
        colorTransitionChild: Colors.black,
        proportionalChildArea: true,
        borderRadius: 50,
        leftAnimationType: InnerDrawerAnimation.static,
        backgroundDecoration: BoxDecoration(color: Colors.white),
        leftChild: Padding(
          padding: const EdgeInsets.symmetric(vertical: 64, horizontal: 15),
          child: Container(
              child: Column(
            children: [
              Material(
                  color: Colors.white,
                  child: ProfileWidget(
                      title:
                          customerInfo == null ? "Dauken" : customerInfo.name,
                      email: customer.email == null
                          ? customer.phoneNumber
                          : customer.email)),
              _menuItem(context, () {}, "Balance: ${customerInfo.balance} \$",
                  Icon(Icons.money)),
              _menuItem(context, () {
                Navigator.pop(context);
                showDialog(
                    context: context,
                    builder: (ctx) {
                      return Dialog(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(16)),
                          backgroundColor: Colors.white,
                          child: _buildDialog(context));
                    });
              }, "Fill balance", Icon(Icons.plus_one)),
              _menuItem(
                  context,
                  () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => HomeScreen())),
                  "Home",
                  Icon(Icons.home)),
              _menuItem(
                  context,
                  () => BlocProvider.of<HomeScreenBloc>(context)
                      .add(LogoutHomeScreenEvent()),
                  "News",
                  Icon(Icons.new_releases)),
              _menuItem(
                  context,
                  () => BlocProvider.of<HomeScreenBloc>(context)
                      .add(LogoutHomeScreenEvent()),
                  "Bets",
                  Icon(Icons.confirmation_number)),
              _menuItem(
                  context,
                  () => BlocProvider.of<HomeScreenBloc>(context)
                      .add(LogoutHomeScreenEvent()),
                  "Friends",
                  Icon(Icons.people_rounded)),
              _menuItem(
                  context,
                  () => BlocProvider.of<HomeScreenBloc>(context)
                      .add(LogoutHomeScreenEvent()),
                  "Cybersport",
                  Icon(Icons.gamepad)),
              _menuItem(
                  context,
                  () => BlocProvider.of<HomeScreenBloc>(context)
                      .add(LogoutHomeScreenEvent()),
                  "Info",
                  Icon(Icons.info)),
              _menuItem(
                  context,
                  () => BlocProvider.of<HomeScreenBloc>(context)
                      .add(LogoutHomeScreenEvent()),
                  "Logout",
                  Icon(Icons.logout))
            ],
          )),
        ),
        scaffold: Scaffold(
            backgroundColor: Colors.white,
            appBar: Widgets().buildAppBar(context, "Home screen"),
            body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 15),
              child: Container(
                  child: Column(
                children: [
                  Expanded(
                    child: ListView.builder(
                        itemCount: articles.length,
                        itemBuilder: (context, index) {
                          return NewsItemWidget(
                            () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => DetailScreen(
                                          articles[index].urlToImage ?? "",
                                          articles[index].title ?? "",
                                          articles[index].publishedAt ?? "",
                                          articles[index].content ??
                                              articles[index].description,
                                          articles[index].source.name ?? "")));
                            },
                            articles[index].title,
                            articles[index].description,
                            articles[index].source.name,
                            DateFormat("yyyy-MM-dd")
                                .format(articles[index].publishedAt),
                          );
                        }),
                  )
                ],
              )),
            )));
  }
}

Widget _customTextField(
  BuildContext context,
  String hintText,
  TextEditingController textEditingController,
  Icon icon,
  bool obscureText,
) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: 10),
    child: TextField(
        inputFormatters: [BlocProvider.of<HomeScreenBloc>(context).formatter],
        obscureText: obscureText,
        controller: textEditingController,
        decoration: InputDecoration(
            prefixIcon: icon,
            hintText: hintText,
            border: OutlineInputBorder())),
  );
}

Widget _buildDialog(BuildContext context) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 18),
    child: Container(
      padding: EdgeInsets.only(top: 25),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("Выберите метод оплаты"),
          SizedBox(
            height: 30,
          ),
          DialogItem(
            icon: Icon(Icons.account_balance),
            title: "Баланс",
            onTap: () {
              // Navigator.pop(context);
              showDialog(
                  context: context,
                  builder: (ctx) {
                    return _buildDialogModal(context);
                  });
            },
            context: context,
          ),
          Divider(color: Colors.black),
          DialogItem(
            icon: Icon(Icons.card_giftcard),
            title: "Банковской картой",
            onTap: () {
              // Navigator.pop(context);
              showDialog(
                  context: context,
                  builder: (ctx) {
                    return _buildDialogModal(context);
                  });
            },
            context: context,
          ),
          Divider(color: Colors.black),
          DialogItem(
            icon: Icon(Icons.computer),
            title: "Платежные терминалы",
            onTap: () {
              // Navigator.pop(context);
              showDialog(
                  context: context,
                  builder: (ctx) {
                    return _buildDialogModal(context);
                  });
            },
            context: context,
          ),
          Divider(color: Colors.black),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    ),
  );
}

Widget _buildDialogModal(BuildContext context) {
  return Dialog(
      child: Container(
    padding: EdgeInsets.symmetric(vertical: 28, horizontal: 18),
    height: 350,
    child: Center(
        child: Column(children: [
      _customTextField(
          context,
          "Enter sum",
          BlocProvider.of<HomeScreenBloc>(context).moneyController,
          Icon(Icons.money_rounded),
          false),
      InkWell(
          onTap: () {
            Navigator.pop(context);

            BlocProvider.of<HomeScreenBloc>(context)
                .add(FillBalanceScreenEvent());
          },
          child: Container(
              height: 50,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.green, borderRadius: BorderRadius.circular(10)),
              child: Center(child: Text("Send"))))
    ])),
  ));
}

// Widget _buildButton(String text, dynamic function()) {
//   return Container(
//     margin: const EdgeInsets.all(20),
//     decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(5), color: Colors.black),
//     width: double.infinity,
//     child: FlatButton(
//       child: Text(
//         text,
//         style: TextStyle(color: Colors.white, fontSize: 16),
//       ),
//       onPressed: () {
//         function();
//       },
//     ),
//   );
// }

Widget _menuItem(context, Function function, String menuItem, Icon icon) {
  return Material(
    child: InkWell(
      onTap: function,
      child: Container(
        height: 60,
        padding: const EdgeInsets.all(5),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
                bottom: BorderSide(
                    width: 1, color: Colors.grey, style: BorderStyle.solid))),
        child: Row(
          children: [
            icon,
            const SizedBox(
              width: 26,
            ),
            Text(
              menuItem,
              style: const TextStyle(
                  fontFamily: ('SFUIDisplay'),
                  fontSize: 17,
                  fontWeight: FontWeight.w300,
                  color: ColorPalette.mainBlack),
            )
          ],
        ),
      ),
    ),
  );
}

class ProfileWidget extends StatelessWidget {
  final String title;
  final String email;
  const ProfileWidget({Key key, this.title, this.email}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Row(
        children: [
          Icon(Icons.people),
          const SizedBox(
            width: 14,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: ThemeTextStyle.textStyle17w600Black,
              ),
              const SizedBox(
                height: 4,
              ),
              Text(
                email,
                style: const TextStyle(
                    fontFamily: ('SFUIDisplay'),
                    fontSize: 15,
                    fontWeight: FontWeight.normal,
                    color: ColorPalette.colorGrey),
              )
            ],
          )
        ],
      ),
    );
  }
}
