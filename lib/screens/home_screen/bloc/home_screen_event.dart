part of 'home_screen_bloc.dart';

abstract class HomeScreenEvent extends Equatable {
  const HomeScreenEvent();

  @override
  List<Object> get props => [];
}

class InitialHomeScreenEvent extends HomeScreenEvent {}

class LogoutHomeScreenEvent extends HomeScreenEvent {}

class FillBalanceScreenEvent extends HomeScreenEvent {

}
