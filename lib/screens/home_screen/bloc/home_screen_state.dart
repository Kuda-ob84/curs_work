part of 'home_screen_bloc.dart';

abstract class HomeScreenState extends Equatable {
  const HomeScreenState();

  @override
  List<Object> get props => [];
}

class LoadingHomeScreenState extends HomeScreenState {}

class EmptyState extends HomeScreenState {}


class LoadDataHomeScreenState extends HomeScreenState {
  final User user;
  final List<Article> articles;
  final CustomerInfo customer;
  LoadDataHomeScreenState(this.user, this.articles, this.customer);
  @override
  List<Object> get props => [user, articles];
}
