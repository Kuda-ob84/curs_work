import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curs_work/domain/customer.dart';
import 'package:curs_work/domain/customer_info.dart';
import 'package:curs_work/network/news_dto.dart';
import 'package:curs_work/services/auth.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

part 'home_screen_event.dart';
part 'home_screen_state.dart';

class HomeScreenBloc extends Bloc<HomeScreenEvent, HomeScreenState> {
  HomeScreenBloc() : super(LoadingHomeScreenState());
  AuthService _authService = new AuthService();
  String document;
  CustomerInfo userInfo;
  TextEditingController moneyController = TextEditingController();
  MaskTextInputFormatter formatter =
      MaskTextInputFormatter(mask: "#######", filter: {"#": RegExp(r'[0-9]')});
  User customer;
  @override
  Stream<HomeScreenState> mapEventToState(
    HomeScreenEvent event,
  ) async* {
    if (event is InitialHomeScreenEvent)
      yield* _buildInitialHomeScreenEvent();
    else if (event is LogoutHomeScreenEvent)
      yield* _buildLogoutHomeScreenEvent();
    else if (event is FillBalanceScreenEvent)
      yield* _buildFillBalanceScreenEvent();
  }

  // void getUserData() async {
  //   QuerySnapshot q = await FirebaseFirestore.instance.collection("auth").get();
  //   var list = q.docs;
  //   Customer customer;
  //   DocumentReference d =
  //       FirebaseFirestore.instance.collection("auth").doc("${list[0].id}");
  //   print(d);
  //   FirebaseFirestore.instance
  //       .collection("auth")
  //       .doc("${list[0].id}")
  //       .get()
  //       .then((DocumentSnapshot snapshot) {
  //     // prints the document Map{}
  //     debugPrint(snapshot.data.toString());
  //   });
  // }

  // Future<CustomerInfo> getDocs() async {
  //   CustomerInfo customerInfo = CustomerInfo();
  //   String id;
  //   var user = _authService.firebaseAuth.currentUser;
  //   QuerySnapshot querySnapshot =
  //       await FirebaseFirestore.instance.collection("auth").get();
  //   for (int i = 0; i < querySnapshot.docs.length; i++) {
  //     var a = querySnapshot.docs[i];
  //     id = a.id;
  //     var docRef = FirebaseFirestore.instance.collection("auth").doc(id);
  //     docRef.get().then((doc) {
  //       if (doc.data()["uid"] == user.uid) {
  //         customerInfo = CustomerInfo.fromFirebase(doc.data());
  //         print(customerInfo.name);
  //         return customerInfo;
  //       }
  //     });
  //   }
  // }

  Stream<HomeScreenState> _buildInitialHomeScreenEvent() async* {
    // await Future.delayed(Duration(seconds: 5));
    customer = _authService.firebaseAuth.currentUser;
    String id;

    QuerySnapshot querySnapshot =
        await FirebaseFirestore.instance.collection("auth").get();

    for (int i = 0; i < querySnapshot.docs.length; i++) {
      var a = querySnapshot.docs[i];
      id = a.id;
      var docRef = FirebaseFirestore.instance.collection("auth").doc(a.id);
      FirebaseFirestore.instance.collection("auth").doc(a.id).get().then((doc) {
        if (doc.data()["uid"] == customer.uid) {
          document = doc.id;
          userInfo = CustomerInfo.fromFirebase(doc.data());
          print(userInfo.name);
        }
      });
    }

    var dio = Dio();
    var response = await dio.get(
        "http://newsapi.org/v2/top-headlines?country=gb&category=sports&apiKey=e70e4d5cc04a4353b143a96a2d032bff");
    DTOSportNews data = DTOSportNews.fromJson(response.data);
    List<Article> article = data.articles;
    yield LoadDataHomeScreenState(customer, article, userInfo);
  }

  Stream<HomeScreenState> _buildLogoutHomeScreenEvent() async* {
    _authService.logOut();
  }

  Stream<HomeScreenState> _buildFillBalanceScreenEvent() async* {
    try {
      FirebaseFirestore.instance.collection("auth").doc(document).update(
          {"balance": int.parse(moneyController.text) + userInfo.balance});
    } catch (e) {
      print(e.toString());
    }
    yield EmptyState();
    yield LoadingHomeScreenState();

    yield* _buildInitialHomeScreenEvent();
  }
}
