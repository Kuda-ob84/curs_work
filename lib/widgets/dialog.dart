import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DialogItem extends StatelessWidget {
  final Icon icon;
  final String title;
  final Function onTap;
  final BuildContext context;

  const DialogItem({Key key, this.icon, this.title, this.onTap, this.context})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 5),
      child: InkWell(
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Container(
            width: 70,
            height: 70,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(35),
            ),
            child: Center(child: icon),
          ),
          SizedBox(width: 10),
          Expanded(child: Text("$title", style: TextStyle(color: Colors.black)))
        ]),
        onTap: () {
          Navigator.of(context, rootNavigator: true).pop();
          onTap();
        },
      ),
    );
  }
}
