import 'package:curs_work/theme/main_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class EmptyDataWidget extends StatelessWidget {
  final String title;

  EmptyDataWidget(this.title);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            title,
            style: const TextStyle(
                fontFamily: ('SFUIDisplay'),
                fontWeight: FontWeight.normal,
                fontSize: 21,
                color: ColorPalette.mainGrey),
          ),
          SizedBox(
            height: 50,
          ),
          SvgPicture.asset(
            'assets/icons_svg/empty.svg',
            height: 80,
            width: 80,
            color: ColorPalette.colorBorder,
          )
        ],
      ),
    );
  }
}
