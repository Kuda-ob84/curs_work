import 'package:curs_work/theme/main_theme.dart';
import 'package:curs_work/widgets/circular_progress_indicator_widget.dart';
import 'package:flutter/material.dart';

class MainButton extends StatefulWidget {
  final String title;
  final Function onTap;
  final Color color;
  final bool isLoad;
  final double fontSize;
  final int duration;

  MainButton(
    this.title,
    this.onTap,
    this.color, {
    this.isLoad = false,
    this.fontSize = 19,
    this.duration = 0,
  });

  @override
  _MainButtonState createState() => _MainButtonState();
}

class _MainButtonState extends State<MainButton>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    if (widget.duration > 0) {
      controller = AnimationController(
          duration: Duration(seconds: widget.duration), vsync: this);
      animation = Tween(begin: 0.0, end: 1.0).animate(controller)
        ..addListener(() {
          setState(() {
            if (controller.value > (widget.duration * 0.1) - 0.01) {
              controller.stop();
              widget.onTap();
            }
          });
        });
      controller.repeat();
    }
  }

  @override
  void dispose() {
    controller?.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        padding: const EdgeInsets.all(15),
        color: widget.color,
        textColor: Colors.white,
        disabledTextColor: Colors.white10,
        elevation: 3.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        onPressed: widget.onTap,
        child: Stack(
          children: [
            widget.duration > 0
                ? ClipRRect(
                    borderRadius: BorderRadius.circular(6.0),
                    child: Container(
                        child: Center(
                            child: LinearProgressIndicator(
                      minHeight: double.infinity,
                      backgroundColor: widget.color,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Colors.white.withOpacity(0.3)),
                      value: animation.value,
                    ))),
                  )
                : Container(),
            Container(
                child: Center(
                    child: widget.isLoad
                        ? CircularProgressIndicatorWidget(
                            height: 30,
                            width: 30,
                            color: Colors.white,
                          )
                        : Text(
                            widget.title,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontFamily: ('SFProDisplay'),
                              fontWeight: FontWeight.w500,
                              fontSize: widget.fontSize,
                            ),
                          ))),
          ],
        ));
  }
}

class AnimateButton extends StatefulWidget {
  final String title;
  final Function onTap;
  final Color color;
  final bool isLoad;
  final double fontSize;
  final int duration;
  final Function(bool) close;
  bool animate;

  AnimateButton(
    this.title,
    this.onTap,
    this.color, {
    this.isLoad = false,
    this.fontSize = 16,
    this.duration = 10,
    this.animate = false,
    this.close,
  });

  @override
  _AnimateButtonState createState() => _AnimateButtonState();
}

class _AnimateButtonState extends State<AnimateButton>
    with SingleTickerProviderStateMixin {
  double size = 20.0;

  void _updateSize() {
    setState(() {
      size = widget.animate ? 250.0 : 20.0;
    });
  }

  void _close() {
    widget.close(true);
  }

  @override
  void initState() {
    super.initState();
    _updateSize();
    print(size);
  }

  @override
  void setState(fn) {
    super.setState(fn);
    _updateSize();
    print(size);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            widget.animate = !widget.animate;
            print(widget.animate);
          },
          child: Container(
              height: 50,
              decoration: BoxDecoration(
                  color: ColorPalette.mainGreen,
                  borderRadius: BorderRadius.all(Radius.circular(6))),
              child: Center(
                  child: widget.isLoad
                      ? CircularProgressIndicatorWidget(
                          height: 30,
                          width: 30,
                          color: Colors.white,
                        )
                      : Text(
                          widget.title,
                          style: TextStyle(
                              fontFamily: ('SFProDisplay'),
                              fontWeight: FontWeight.w300,
                              fontSize: widget.fontSize,
                              color: Colors.white),
                        ))),
        ),
        GestureDetector(
          onTap: widget.onTap,
          child: AnimatedSize(
            curve: Curves.easeIn,
            alignment: Alignment.centerLeft,
            vsync: this,
            duration: Duration(seconds: 10),
            child: Container(
              height: 50,
              width: size,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
                gradient: LinearGradient(
                  colors: <Color>[
                    Colors.transparent,
                    Colors.white.withOpacity(0.5)
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
