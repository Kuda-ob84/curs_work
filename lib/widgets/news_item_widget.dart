import 'dart:ui';

import 'package:curs_work/theme/main_theme.dart';
import 'package:flutter/material.dart';

class NewsItemWidget extends StatelessWidget {
  final Function onTap;
  final String title;
  final String description;
  final String category;
  final String date;

  NewsItemWidget(
      this.onTap, this.title, this.description, this.category, this.date);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(28),
        margin: const EdgeInsets.only(bottom: 12),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/background.jpg"), fit: BoxFit.cover),
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                color: ColorPalette.mainBlack.withOpacity(0.08),
                blurRadius: 15,
                offset: Offset(0, 1),
              )
            ]),
        child: ClipRRect(
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
            child: Column(
              children: [
                Text(
                  title,
                  style: TextStyle(
                      color: ColorPalette.mainWhite,
                      fontWeight: FontWeight.w700,
                      fontFamily: ('SFUIDisplay'),
                      fontSize: 17),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  description,
                  style: const TextStyle(
                      fontFamily: ('SFUIDisplay'),
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: ColorPalette.mainWhite),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      category,
                      style: const TextStyle(
                          fontFamily: ('SFUIDisplay'),
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          color: ColorPalette.colorAlert),
                    ),
                    Text(
                      date,
                      style: ThemeTextStyle.textStyle13w500Grey,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
