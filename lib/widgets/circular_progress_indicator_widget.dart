import 'package:curs_work/theme/main_theme.dart';
import 'package:flutter/material.dart';

class CircularProgressIndicatorWidget extends StatelessWidget {
  final Color color;
  final Color backgroundColor;
  final double width;
  final double height;

  CircularProgressIndicatorWidget({
    this.color: ColorPalette.mainGreen,
    this.backgroundColor: Colors.transparent,
    this.width,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      color: backgroundColor,
      child: Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.grey.withOpacity(0.5),
          strokeWidth: 1,
          valueColor: AlwaysStoppedAnimation<Color>(color),
        ),
      ),
    );
  }
}
