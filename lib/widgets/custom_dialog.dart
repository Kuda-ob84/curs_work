import 'dart:ui';

import 'package:curs_work/theme/main_theme.dart';
import 'package:flutter/material.dart';

import 'buttons.dart';

class CustomDialog extends StatelessWidget {
  final bool isPositive;
  final String dialogContent;
  final Function onButtonTapFunction;
  final bool showDontButton;
  final int duration;

  CustomDialog(this.isPositive, this.dialogContent,
      {this.onButtonTapFunction, this.showDontButton: false, this.duration});

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 1.0, sigmaY: 1.0),
      child: Container(
        color: Colors.black.withOpacity(0.2),
        child: Dialog(
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Container(
            padding: EdgeInsets.all(8),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: isPositive == true
                        ? ColorPalette.mainGreen
                        : Colors.redAccent,
                  ),
                  child: Center(
                    child: Icon(
                      isPositive == true ? Icons.check : Icons.clear,
                      size: 32,
                      color: Colors.white,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  dialogContent,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    color: ColorPalette.mainGrey,
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                const SizedBox(
                  height: 20,
                ),
                MainButton(
                  'OK',
                  onButtonTapFunction != null
                      ? onButtonTapFunction
                      : () => Navigator.pop(context),
                  isPositive ? ColorPalette.mainGreen : Colors.redAccent,
                  duration: duration,
                ),
                SizedBox(
                  height: showDontButton ? 20 : 0,
                ),
                showDontButton
                    ? GestureDetector(
                        onTap: () => Navigator.pop(context),
                        child: Container(
                          padding: const EdgeInsets.all(10.0),
                          width: double.infinity,
                          child: Text(
                            'Нет',
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              color: ColorPalette.mainGrey,
                              fontSize: 16,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                      )
                    : SizedBox(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class AnimateCustomDialog extends StatefulWidget {
  final bool isPositive;
  final String dialogContent;
  final Function onButtonTapFunction;
  final bool showDontButton;
  final bool animate;
  final int duration;

  AnimateCustomDialog(this.isPositive, this.dialogContent,
      {this.onButtonTapFunction,
      this.showDontButton: false,
      this.animate = false,
      this.duration = 10});

  @override
  _AnimateCustomDialogState createState() => _AnimateCustomDialogState();
}

class _AnimateCustomDialogState extends State<AnimateCustomDialog> {
  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 1.0, sigmaY: 1.0),
      child: Container(
        color: Colors.black.withOpacity(0.2),
        child: Dialog(
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Container(
            padding: EdgeInsets.all(kHorizontalPadding),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: widget.isPositive == true
                        ? ColorPalette.mainGreen
                        : Colors.redAccent,
                  ),
                  child: Center(
                    child: Icon(
                      widget.isPositive == true ? Icons.check : Icons.clear,
                      size: 32,
                      color: Colors.white,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  widget.dialogContent,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    color: ColorPalette.mainGrey,
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                AnimateButton(
                  'OK',
                  widget.onButtonTapFunction != null
                      ? widget.onButtonTapFunction
                      : () => Navigator.pop(context),
                  widget.isPositive ? ColorPalette.mainGreen : Colors.redAccent,
                  animate: widget.animate,
                  duration: widget.duration,
                  close: (value) {
                    if (value) {
                      Navigator.pop(context);
                    }
                  },
                ),
                SizedBox(
                  height: widget.showDontButton ? 20 : 0,
                ),
                widget.showDontButton
                    ? GestureDetector(
                        onTap: () => Navigator.pop(context),
                        child: Container(
                          padding: const EdgeInsets.all(10.0),
                          width: double.infinity,
                          child: Text(
                            'Нет',
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              color: ColorPalette.mainGrey,
                              fontSize: 16,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                      )
                    : SizedBox(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class RateDialog extends StatefulWidget {
  final Function(double) onRated;

  RateDialog({this.onRated});

  @override
  _RateDialogState createState() => _RateDialogState();
}

class _RateDialogState extends State<RateDialog> {
  double userRating = 0.0;

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 1.0, sigmaY: 1.0),
      child: Container(
        color: Colors.black.withOpacity(0.35),
        child: Dialog(
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Container(
            padding: EdgeInsets.all(kHorizontalPadding),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const SizedBox(
                  height: 36,
                ),
                Text(
                  'Оцените наше приложение',
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    color: Color(0xFF111111),
                    fontSize: 22,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(
                  height: 11,
                ),
                Text(
                  'Это приложение было сделано для вас. Мы ценим ваше мнение и отзывы',
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    color: Color(0xFF474747),
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
