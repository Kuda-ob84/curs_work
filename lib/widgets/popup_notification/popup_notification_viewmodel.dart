class PopupNotificationViewModel {
  String title;
  String body;

  PopupNotificationViewModel(Map<String, dynamic> message) {
    title = message['notification']['title'] ?? '';
    body = message['notification']['body'] ?? '';
  }
}
