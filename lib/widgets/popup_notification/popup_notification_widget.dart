import 'dart:async';

import 'package:flutter/material.dart';

import 'popup_notification_viewmodel.dart';

class PopupNotification extends StatefulWidget {
  PopupNotification({Key key}) : super(key: key);

  @override
  _PopupNotificationState createState() => _PopupNotificationState();
}

class _PopupNotificationState extends State<PopupNotification> {
  Timer _timer;
  String _title = '';
  String _body = '';
  bool _visible = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: _visible,
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: GestureDetector(
            onTapDown: (tapDownDetails) {
              _timer?.cancel();
            },
            onTapUp: (tapUpDetails) {
              _timer = Timer(Duration(seconds: 1), _close);
            },
            child: Dismissible(
              direction: DismissDirection.up,
              key: UniqueKey(),
              onDismissed: (dismissDirection) {
                _close();
              },
              child: Container(
                padding: const EdgeInsets.all(16),
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(6),
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 6,
                      spreadRadius: 6,
                      color: Colors.black38,
                    ),
                  ],
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _title,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        fontFamily: ('SFProDisplay'),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      _body,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.normal,
                        fontFamily: ('SFProDisplay'),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _show(PopupNotificationViewModel message) {
    setState(() {
      _title = message.title;
      _body = message.body;
      _visible = true;
    });
    _timer?.cancel();
    _timer = Timer(Duration(seconds: 4), _close);
  }

  void _close() {
    setState(() {
      _visible = false;
    });
  }
}
