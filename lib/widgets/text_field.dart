import 'package:curs_work/theme/main_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

class TextFieldWithLabelText extends StatefulWidget {
  final TextEditingController textController;
  final TextInputType keyboardType;
  final TextInputFormatter textInputFormatter;
  final FocusNode focusNode;
  final String labelText;
  final String hintText;
  final bool showPassword;
  final bool clear;
  final Function func;
  final String error;
  final Function onChanged;
  final Function changeFocusNode;
  final bool isPossibleError;
  final Function(String text) validate;
  final GlobalKey<FormState> globalKey;
  final String prefixIcon;

  TextFieldWithLabelText(
      {Key key,
      @required this.textController,
      this.keyboardType,
      this.textInputFormatter,
      @required this.focusNode,
      this.labelText,
      this.hintText,
      this.showPassword,
      this.clear,
      this.func,
      this.error,
      this.onChanged,
      this.changeFocusNode,
      this.isPossibleError = true,
      this.validate,
      @required this.globalKey,
      this.prefixIcon})
      : assert(globalKey != null),
        assert(textController != null),
        assert(focusNode != null),
        super(key: key);

  @override
  _TextFieldWithLabelTextState createState() => _TextFieldWithLabelTextState();
}

class _TextFieldWithLabelTextState extends State<TextFieldWithLabelText> {
  bool showPasswordIcon;

  bool showClearButton = false;

  void setStateIfMounted(f) {
    if (mounted) setState(f);
  }

  @override
  void initState() {
    if (widget.showPassword != null) {
      widget.textController.addListener(() {
        if (widget.textController.text.isNotEmpty) {
          setStateIfMounted(() {
            showPasswordIcon = widget.showPassword;
          });
        } else {
          if (this.mounted) {
            setStateIfMounted(() {
              showPasswordIcon = null;
            });
          }
        }
      });
    }

    if (widget.clear != null || widget.clear == true) {
      widget.textController.addListener(() {
        if (widget.textController.text.isNotEmpty) {
          setStateIfMounted(() {
            showClearButton = true;
          });
        } else {
          setStateIfMounted(() {
            showClearButton = false;
          });
        }
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          height: 48,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
                bottom: BorderSide(
                    color: ColorPalette.colorBorder,
                    width: 1,
                    style: BorderStyle.solid)),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              SvgPicture.asset(
                'assets/icons_svg/${widget.prefixIcon}.svg',
                color: ColorPalette.mainGreen,
              ),
              SizedBox(
                width: 14,
              ),
              Expanded(
                child: Form(
                  key: widget.globalKey,
                  child: TextFormField(
                    maxLines: 1,
                    validator: (value) => widget.validate(value),
                    onChanged: widget.onChanged,
                    keyboardType: widget.keyboardType != null
                        ? widget.keyboardType
                        : TextInputType.visiblePassword,
                    controller: widget.textController,
                    inputFormatters: widget.textInputFormatter != null
                        ? [widget.textInputFormatter]
                        : null,
                    onFieldSubmitted: (value) {
                      widget.focusNode.unfocus();
                      widget.changeFocusNode.call();
                    },
                    style: const TextStyle(
                        fontWeight: FontWeight.w400,
                        fontFamily: 'SFUIDisplay',
                        fontSize: 15,
                        color: ColorPalette.mainGrey),
                    focusNode: widget.focusNode,
                    autocorrect: false,
                    obscureText: widget.showPassword != null
                        ? !widget.showPassword
                        : false,
                    // textAlignVertical: TextAlignVertical.center,
                    decoration: InputDecoration(
                      // fillColor: Colors.orangeAccent,
                      // filled: true,
                      hintText: widget.hintText,
                      labelText: widget.labelText,
                      labelStyle: const TextStyle(
                        fontFamily: ('SFUIDisplay'),
                        fontWeight: FontWeight.w400,
                        fontSize: 15,
                        color: ColorPalette.mainGrey,
                      ),
                      // alignLabelWithHint: true,
                      suffixIcon: showClearButton
                          ? InkWell(
                              onTap: widget.func,
                              child: Container(
                                margin: EdgeInsets.all(10),
                                child: SvgPicture.asset(
                                  "assets/icons_svg/close.svg",
                                  height: 10,
                                  width: 10,
                                ),
                              ),
                            )
                          : showPasswordIcon != null
                              ? InkWell(
                                  onTap: widget.func,
                                  child: Container(
                                    margin: EdgeInsets.all(10),
                                    child: SvgPicture.asset(
                                      "assets/icons_svg/eye${widget.showPassword ? "" : "hidden"}.svg",
                                    ),
                                  ),
                                )
                              : null,
                      hintStyle: const TextStyle(
                        fontFamily: ('SFUIDisplay'),
                        fontWeight: FontWeight.w400,
                        fontSize: 15,
                        color: ColorPalette.mainGrey,
                      ),
                      // contentPadding: EdgeInsets.only(top: 10, bottom: 10),
                      border: InputBorder.none,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
            top: 5,
          ),
          child: widget.isPossibleError == true
              ? Text(
                  widget.error != null ? widget.error : '',
                  style: const TextStyle(
                    color: Colors.red,
                  ),
                )
              : Container(),
        )
      ],
    );
  }
}

class CustomTextField extends StatefulWidget {
  final TextEditingController textEditingController;
  final TextInputType textInputType;
  final FocusNode focusNode;
  final String hintText;
  final int maxLines;
  final int minLines;
  final Function onSubmitted;
  final Color color;
  final String suffixIcon;
  final Function onTap;
  final double padding;

  CustomTextField({
    Key key,
    @required this.textEditingController,
    this.textInputType: TextInputType.visiblePassword,
    @required this.focusNode,
    this.hintText,
    this.maxLines,
    this.minLines,
    this.onSubmitted,
    this.color: Colors.white,
    this.suffixIcon = '',
    this.onTap,
    this.padding = 0,
  })  : assert(textEditingController != null),
        assert(focusNode != null),
        super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: widget.textInputType,
      onSubmitted: widget.onSubmitted,
      maxLines: widget.maxLines,
      minLines: widget.minLines,
      focusNode: widget.focusNode,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(widget.padding),
        filled: true,
        fillColor: widget.color,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: widget.color, width: 1),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: widget.color, width: 1),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: widget.color, width: 1),
        ),
        border: OutlineInputBorder(),
        suffixIcon: InkWell(
            onTap: widget.onTap,
            child: Container(
              padding: const EdgeInsets.all(12),
              child: SvgPicture.asset(
                widget.suffixIcon,
              ),
            )),
        hintText: widget.hintText,
        hintStyle: const TextStyle(
          fontSize: 14,
          fontFamily: 'SFUIDisplay',
          fontWeight: FontWeight.w400,
          color: ColorPalette.mainGrey,
        ),
      ),
      controller: widget.textEditingController,
    );
  }
}
