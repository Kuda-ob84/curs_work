import 'package:curs_work/theme/main_theme.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class SmsCodeTextfield extends StatefulWidget {
  final BuildContext context;
  final TextEditingController textEditingController;
  final FocusNode focusNode;
  final GlobalKey<FormState> globalKey;
  final Function(String text) onFieldSubmitted;
  final Function(String text) validate;
  final Function(String text) onFieldChanged;
  final Function(String text) onFieldCompleted;
  final int lengthSymbol;

  SmsCodeTextfield({
    Key key,
    @required this.context,
    @required this.textEditingController,
    @required this.focusNode,
    @required this.globalKey,
    @required this.onFieldChanged,
    this.onFieldSubmitted,
    this.validate,
    this.onFieldCompleted,
    this.lengthSymbol = 4,
  })  : assert(context != null),
        assert(textEditingController != null),
        assert(focusNode != null),
        assert(globalKey != null),
        assert(onFieldChanged != null),
        super(key: key);

  @override
  _SmsCodeTextfieldState createState() => _SmsCodeTextfieldState();
}

class _SmsCodeTextfieldState extends State<SmsCodeTextfield> {
  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget.globalKey,
      child: Container(
        child: PinCodeTextField(
          appContext: widget.context,
          controller: widget.textEditingController,
          focusNode: widget.focusNode,
          keyboardType: TextInputType.number,
          length: widget.lengthSymbol,
          autoDisposeControllers: false,
          animationType: AnimationType.fade,
          animationDuration: Duration(milliseconds: 150),
          backgroundColor: Colors.transparent,
          enableActiveFill: true,
          enablePinAutofill: true,
          pinTheme: PinTheme.defaults(
            fieldHeight: 58,
            fieldWidth: 53,
            shape: PinCodeFieldShape.box,
            borderWidth: 0.0,
            borderRadius: BorderRadius.all(Radius.circular(8)),
            activeColor: ColorPalette.colorUltraLightGrey,
            inactiveColor: ColorPalette.colorUltraLightGrey,
            disabledColor: ColorPalette.colorUltraLightGrey,
            inactiveFillColor: ColorPalette.colorUltraLightGrey,
            activeFillColor: ColorPalette.colorUltraLightGrey,
            selectedFillColor: ColorPalette.colorUltraLightGrey_2,
            selectedColor: ColorPalette.colorUltraLightGrey,
          ),
          textStyle: TextStyle(
            fontSize: 32,
            fontWeight: FontWeight.w600,
            fontFamily: 'SFProDisplay',
          ),
          validator: (text) => widget.validate(text),
          onSubmitted: (text) => widget.onFieldSubmitted(text),
          onChanged: (text) => widget.onFieldChanged(text),
          onCompleted: (text) => widget.onFieldCompleted(text),
        ),
      ),
    );
  }
}
