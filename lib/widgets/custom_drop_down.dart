import 'package:curs_work/theme/main_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'dropdown_widget/dropdown_model.dart';

class CustomDropDown extends StatefulWidget {
  final Function(String) selectedTap;
  final Function(int) onTap;
  final List<DropdownModel> valueList;
  final String labelText;

  CustomDropDown(
      {this.selectedTap, this.onTap, this.valueList, this.labelText});

  @override
  _CustomDropDownState createState() => _CustomDropDownState();
}

class _CustomDropDownState extends State<CustomDropDown> {
  @override
  Widget build(BuildContext context) {
    int id = widget.valueList.first.id;
    return Container(
      padding: EdgeInsets.only(
          left: 17,
          top: widget.labelText == '' ? 0 : 11,
          bottom: widget.labelText == '' ? 0 : 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: ColorPalette.mainBlack.withOpacity(0.08),
              blurRadius: 15,
              offset: Offset(0, 1),
            )
          ]),
      child: DropdownButtonHideUnderline(
        child: DropdownButtonFormField(
          decoration: InputDecoration(
            contentPadding: EdgeInsets.zero,
            labelText: widget.labelText == '' ? null : widget.labelText,
            labelStyle: ThemeTextStyle.textStyle13w400Grey,
            enabled: true,
            enabledBorder: InputBorder.none,
            border: InputBorder.none,
          ),
          value: widget.valueList.first.name,
          isDense: false,
          isExpanded: true,
          icon: Padding(
            padding: const EdgeInsets.only(right: 18),
            child: SvgPicture.asset('assets/icons_svg/arrow_down.svg'),
          ),
          onChanged: (newValue) {
            setState(() {
              widget.selectedTap(newValue);
              widget.onTap(id);
            });
          },
          items: widget.valueList.map((dropdownItem) {
            return DropdownMenuItem<String>(
              value: dropdownItem.name,
              onTap: () {
                id = dropdownItem.id;
              },
              child: Text(
                dropdownItem.name,
                style: const TextStyle(
                  fontSize: 15,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w500,
                  color: ColorPalette.mainBlack,
                  fontFamily: 'SfUiDisplay',
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}
