class DropdownModel {
  final int id;
  final String name;
  final String sysName;

  DropdownModel({this.id = 0, this.name = "", this.sysName = ""});
}
