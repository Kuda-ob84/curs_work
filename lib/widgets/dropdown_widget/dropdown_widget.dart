import 'package:curs_work/theme/main_theme.dart';
import 'package:curs_work/widgets/dropdown_widget/dropdown_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Dropdown extends StatefulWidget {
  final Function(int dropdownItem) onChangeDropdownItem;
  final List<DropdownModel> dropdownModelsList;
  final String initialDropdownItem;
  final String hintText;
  final bool setFirstValueSelected;
  final bool isError;
  final Function onTapDropdown;

  Dropdown({
    Key key,
    @required this.onChangeDropdownItem,
    @required this.dropdownModelsList,
    this.initialDropdownItem,
    @required this.hintText,
    this.setFirstValueSelected: false,
    this.isError: false,
    this.onTapDropdown,
  })  : assert(onChangeDropdownItem != null),
        assert(dropdownModelsList != null),
        assert(hintText != null),
        super(key: key);

  @override
  _DropdownState createState() => _DropdownState(initialDropdownItem);
}

class _DropdownState extends State<Dropdown> {
  String currentSelectedDropdownItem;
  int currentId;

  _DropdownState(this.currentSelectedDropdownItem);

  @override
  void initState() {
    if (widget.setFirstValueSelected) {
      currentSelectedDropdownItem = widget.dropdownModelsList.first.name;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 21),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        border: Border.all(
            color: widget.isError ? Colors.red : ColorPalette.colorBorder,
            width: 1,
            style: BorderStyle.solid),
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<String>(
          onTap: widget.onTapDropdown,
          hint: Text(
            widget.hintText,
            style: const TextStyle(
              fontFamily: 'SFUIDisplay',
              fontSize: 16,
              color: ColorPalette.mainGrey,
            ),
          ),
          value: currentSelectedDropdownItem,
          isDense: true,
          isExpanded: true,
          icon: SvgPicture.asset('assets/icons_svg/arrow_down.svg'),
          onChanged: (selectedDropdownItem) {
            setState(() {
              currentSelectedDropdownItem = selectedDropdownItem;
              widget.onChangeDropdownItem(currentId);
            });
          },
          items: widget.dropdownModelsList.map((dropdownItem) {
            currentId = (dropdownItem.id);
            return DropdownMenuItem<String>(
              value: dropdownItem.name,
              child: Text(
                dropdownItem.name,
                style: const TextStyle(
                  fontFamily: 'SFProDisplay',
                  fontSize: 16,
                  color: Colors.black,
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}
