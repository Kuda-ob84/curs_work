import 'package:curs_work/theme/main_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SliverAppBarWidget extends StatelessWidget {
  final String title;
  final bool viewLeading;

  SliverAppBarWidget(this.title, {this.viewLeading = false});

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      leading: viewLeading
          ? IconButton(
              icon: SvgPicture.asset(
                'assets/icons_svg/arrow_back.svg',
                color: Colors.white,
              ),
              onPressed: () => Navigator.pop(context),
            )
          : SizedBox(),
      backgroundColor: ColorPalette.mainGreen,
      centerTitle: true,
      title: Text(
        title,
        style: ThemeTextStyle.appBarThemeText,
      ),
    );
  }
}
