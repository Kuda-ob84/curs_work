import 'package:curs_work/theme/main_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Widgets {
  String validatePhone(String text) {
    String errorMessage;
    if ((text ?? '').isNotEmpty) {
      if (text.length != 18) {
        errorMessage = 'Телефон заполнен неправильно';
      }
    } else {
      errorMessage = 'Введите телефон';
    }
    return errorMessage;
  }

  String validatePassword(String text) {
    String errorMessage;
    if ((text ?? '').isEmpty) {
      errorMessage = 'Введите пароль';
    }
    return errorMessage;
  }

  Widget circularProgressIndicator({Color color = ColorPalette.mainGreen}) {
    return CircularProgressIndicator(
      backgroundColor: Colors.grey.withOpacity(0.5),
      strokeWidth: 1,
      valueColor: AlwaysStoppedAnimation<Color>(color),
    );
  }

  Widget buildLoadingWidget() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
      ),
      child: Center(
        child: circularProgressIndicator(color: ColorPalette.mainGreen),
      ),
    );
  }

  Widget buildAppBar(BuildContext context, String title,
      {bool showLeading = false, List<Widget> actionWidget}) {
    return AppBar(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(18),
        ),
      ),
      elevation: 0.0,
      leading: showLeading
          ? IconButton(
              icon: SvgPicture.asset(
                'assets/icons_svg/arrow_back.svg',
                color: Colors.white,
              ),
              onPressed: () => Navigator.pop(context),
            )
          : SizedBox(),
      backgroundColor: ColorPalette.mainDarkBlue,
      centerTitle: true,
      title: Text(
        title,
        style: ThemeTextStyle.appBarThemeText,
      ),
      actions: actionWidget,
    );
  }

  Widget buildBody(double height, Widget widget) {
    return Container(
        height: height,
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(
                width: 1, color: Colors.white, style: BorderStyle.solid),
            borderRadius:
                BorderRadius.vertical(top: const Radius.circular(18))),
        child: widget);
  }

  // Widget sliderPhotos(List<String> images) {
  //   return images.length != 0
  //       ? Container(
  //           height: 130,
  //           child: ListView.builder(
  //             scrollDirection: Axis.horizontal,
  //             itemCount: images.length,
  //             itemBuilder: (context, index) {
  //               return GestureDetector(
  //                 onTap: () {
  //                   open(context, index, images);
  //                 },
  //                 child: Container(
  //                   margin: index == 0
  //                       ? index == images.length - 1
  //                           ? const EdgeInsets.only(left: 20, right: 20)
  //                           : const EdgeInsets.only(left: 20, right: 10)
  //                       : index == images.length - 1
  //                           ? const EdgeInsets.only(right: 20)
  //                           : const EdgeInsets.only(right: 10),
  //                   height: 100,
  //                   width: 180,
  //                   decoration: BoxDecoration(
  //                       borderRadius: BorderRadius.all(Radius.circular(10)),
  //                       image: DecorationImage(
  //                           fit: BoxFit.cover,
  //                           image: NetworkImage(
  //                               '${UrlManager.shared.baseURL}/${images[index]}'))),
  //                 ),
  //               );
  //             },
  //           ),
  //         )
  //       : SizedBox();
  // }

  // Widget simplePhoto(BuildContext context, List<String> images) {
  //   return images.length != 0
  //       ? GestureDetector(
  //           onTap: () {
  //             open(context, 0, images);
  //           },
  //           child: Container(
  //             height: 220,
  //             margin: EdgeInsets.symmetric(horizontal: 0),
  //             decoration: BoxDecoration(
  //                 borderRadius: BorderRadius.all(Radius.circular(10)),
  //                 image: DecorationImage(
  //                     fit: BoxFit.cover,
  //                     image: NetworkImage('${images[0]}'
  //                         // '${UrlManager.shared.baseURL}/${images[0]}'
  //                         ))),
  //           ),
  //         )
  //       : SizedBox();
  // }

  Future<bool> showToast(String title) {
    return Fluttertoast.showToast(
        msg: title,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        fontSize: 17,
        timeInSecForIosWeb: 5,
        backgroundColor: Colors.grey[300],
        textColor: Colors.black);
  }
}

class SlideRightRoute extends PageRouteBuilder {
  final Widget page;

  SlideRightRoute({this.page})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) =>
              SlideTransition(
            position: Tween<Offset>(
              begin: const Offset(1, 0),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          ),
        );
}
