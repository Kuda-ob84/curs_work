import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class ColorPalette {
  ///Основные цвета
  static const Color mainWhite = Colors.white;
  static const Color mainGreen = Color(0xFF00ACA0);
  static const Color mainGrey = Color(0xFF545959);
  static const Color mainBlack = Color(0xFF151522);
  static const Color mainDarkBlue = Color(0xFF34495E);

  static const Color backgroundColor = Color(0xFFEEF3F3);

  static const Color colorBorder = Color(0xFFE0E0E0);
  static const Color colorLightGrey = Color(0xFFBDBDBD);
  static const Color colorUltraLightGrey = Color(0xFFF5F5F5);
  static const Color colorUltraLightGrey_2 = Color(0xFFececec);
  static const Color colorUltraLightBlue = Color(0xFFEEF3F3);
  static const Color colorAlert = Color(0xFFEB5757);
  static const Color colorChip = Color(0xFF95A3A2);
  static const Color colorGreen = Color(0xFF27AE60);
  static const Color colorGrey = Color(0xFF828282);
  static const Color colorOrange = Color(0xFFF2994A);
  static const Color colorYellow = Color(0xFFF2C94C);
  static const Color colorPurple = Color(0xFFBB6BD9);
  static const Color colorBlue = Color(0xFF56CCF2);
  static const Color colorLightGreen = Color(0xFF6FCF97);
}

const double kHorizontalPadding = 16.0;
const double kVerticalPadding = 15.0;

class ThemeTextStyle {
  /// fontSize: 14,

  static var textStyle14w400Black = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.normal,
    fontFamily: ('SFUIDisplay'),
    color: ColorPalette.mainBlack,
  );

  /// fontSize: 17,

  static var textStyle17w400Black = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.normal,
    fontFamily: ('SFUIDisplay'),
    color: ColorPalette.mainBlack,
  );
  static var textStyle17w400Green = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.normal,
    fontFamily: ('SFUIDisplay'),
    color: ColorPalette.mainGreen,
  );
  static var textStyle14w400Grey = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.normal,
    fontFamily: ('SFUIDisplay'),
    color: ColorPalette.mainGrey,
  );
  static var textStyle17w500Black = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w500,
    fontFamily: ('SFUIDisplay'),
    color: ColorPalette.mainBlack,
  );
  static var textStyle17w400Grey = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w400,
    fontFamily: ('SFUIDisplay'),
    color: ColorPalette.colorGrey,
  );
  static var textStyle17w500Green = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w500,
    fontFamily: ('SFUIDisplay'),
    color: ColorPalette.mainGreen,
  );
  static var appBarThemeText = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w600,
    fontFamily: ('SFUIDisplay'),
    color: Colors.white,
  );
  static var textStyle17w600Black = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w600,
    fontFamily: ('SFUIDisplay'),
    color: ColorPalette.mainBlack,
  );
  static var textStyle17w600Grey = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w600,
    fontFamily: ('SFUIDisplay'),
    color: ColorPalette.mainGrey,
  );
  static var textStyle17w600Green = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w600,
    fontFamily: ('SFUIDisplay'),
    color: ColorPalette.mainGreen,
  );
  static var textStyle17w700Black = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w700,
    fontFamily: ('SFUIDisplay'),
    color: ColorPalette.mainBlack,
  );
  static var textStyle17w700Grey = TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w700,
    fontFamily: ('SFUIDisplay'),
    color: ColorPalette.colorGrey,
  );

  /// fontSize: 13,

  static var textStyle13w500Grey = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w500,
    fontFamily: ('SFUIDisplay'),
    color: ColorPalette.colorGrey,
  );
  static var textStyle13w400Grey = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.normal,
    fontFamily: ('SFUIDisplay'),
    color: ColorPalette.colorGrey,
  );
}

class FormatsTextTheme {
  var maskFormatter = MaskTextInputFormatter(
      mask: '+7 (###) ###-##-##', filter: {"#": RegExp(r'[0-9]')});
}

class RegExpTheme {
  RegExp mailRegex = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
}
